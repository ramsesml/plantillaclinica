CREATE TABLE PACIENTE (
    PACIENTE_ID int not null identity(1,1) primary key,
    NOMBRE varchar(255),
    APELLIDO varchar(255),
    TELEFONO varchar(50),
    CORREO varchar(255)
);

CREATE TABLE TIPO_CITA (
    TIPO_CITA_ID int not null identity(1,1) primary key,
    TIPO varchar(100)
);

CREATE TABLE CITA (
	CITA_ID int not null identity(1,1) primary key,
    PACIENTE_ID int not null,
	TIPO_CITA_ID int not null,
    FECHA date,
	HORA time,
	CONSTRAINT FK_CITA_PACIENTE FOREIGN KEY (PACIENTE_ID)
    REFERENCES PACIENTE(PACIENTE_ID),
	CONSTRAINT FK_CITA_TIPO_CITA FOREIGN KEY (TIPO_CITA_ID)
    REFERENCES TIPO_CITA(TIPO_CITA_ID)
);


USE [PlantillaClinica]
GO

INSERT INTO [dbo].[PACIENTE]
           ([NOMBRE]
           ,[APELLIDO]
           ,[TELEFONO]
           ,[CORREO])
     VALUES
           ('RAMSES'
           ,'MARIN LEANDRO'
           ,'88793358'
           ,'ramsesml@gmail.com')
GO

INSERT INTO [dbo].[TIPO_CITA]
           ([TIPO])
     VALUES
           ('Medicina General')
GO

INSERT INTO [dbo].[TIPO_CITA]
           ([TIPO])
     VALUES
           ('Odontología')
GO

INSERT INTO [dbo].[TIPO_CITA]
           ([TIPO])
     VALUES
           ('Pediatría')
GO

INSERT INTO [dbo].[TIPO_CITA]
           ([TIPO])
     VALUES
           ('Neurología')
GO