﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PlantillaClinicaClient.Models;
using System.Net.Http;

namespace PlantillaClinicaClient.Controllers
{
    public class TIPO_CITAController : Controller
    {

        // GET: TIPO_CITA
        public ActionResult Index()
        {
            IEnumerable<TIPO_CITA> tipocita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TipoCitas");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<TIPO_CITA>>();
                    readTask.Wait();

                    tipocita = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    tipocita = Enumerable.Empty<TIPO_CITA>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(tipocita);
        }


        // GET: TIPO_CITA/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TIPO_CITA/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TIPO_CITA_ID,TIPO")] TIPO_CITA tIPO_CITA)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP POST
                    var postTask = client.PostAsJsonAsync<TIPO_CITA>("TipoCitas", tIPO_CITA);
                    postTask.Wait();

                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(tIPO_CITA);
        }

        // GET: TIPO_CITA/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TIPO_CITA tipocita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TipoCitas/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<TIPO_CITA>();
                    readTask.Wait();

                    tipocita = readTask.Result;
                }
            }

            return View(tipocita);
        }

        // POST: TIPO_CITA/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TIPO_CITA_ID,TIPO")] TIPO_CITA tIPO_CITA)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP POST
                    var putTask = client.PutAsJsonAsync<TIPO_CITA>("TipoCitas", tIPO_CITA);
                    putTask.Wait();

                    var result = putTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        return RedirectToAction("Index");
                    }
                }
            }
            return View(tIPO_CITA);
        }

        // GET: TIPO_CITA/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TIPO_CITA tipocita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TipoCitas/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<TIPO_CITA>();
                    readTask.Wait();

                    tipocita = readTask.Result;
                }
            }

            return View(tipocita);
        }

        // POST: TIPO_CITA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("TipoCitas/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
    }
}
