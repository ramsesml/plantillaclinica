﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PlantillaClinicaClient.Models;
using System.Net.Http;

namespace PlantillaClinicaClient.Controllers
{
    public class CITAController : Controller
    {

        // GET: CITA
        public ActionResult Index()
        {
            IEnumerable<CITA> cita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Citas");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<CITA>>();
                    readTask.Wait();

                    cita = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    cita = Enumerable.Empty<CITA>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(cita);
        }

        // GET: CITA/Create
        public ActionResult Create()
        {
            fillViewbag("Create");
            return View();
        }

        // POST: CITA/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CITA_ID,PACIENTE_ID,TIPO_CITA_ID,FECHA,HORA")] CITA cITA)
        {
            if (ModelState.IsValid && checkCitaSamePaciente(cITA))
            {
                
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP POST
                    var postTask = client.PostAsJsonAsync<CITA>("Citas", cITA);
                    postTask.Wait();

                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            ModelState.AddModelError(string.Empty, "No se puede poner dos citas al mismo dia para un paciente");
            fillViewbag("Create");
            return View(cITA);
        }

        // GET: CITA/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CITA cita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Citas/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<CITA>();
                    readTask.Wait();

                    cita = readTask.Result;
                }
            }
            fillViewbag("Edit",cita.PACIENTE_ID,cita.TIPO_CITA_ID);
            return View(cita);
        }

        // POST: CITA/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CITA_ID,PACIENTE_ID,TIPO_CITA_ID,FECHA,HORA")] CITA cITA)
        {
            if (ModelState.IsValid && checkCitaSamePaciente(cITA))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP POST
                    var putTask = client.PutAsJsonAsync<CITA>("Citas", cITA);
                    putTask.Wait();

                    var result = putTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        return RedirectToAction("Index");
                    }
                }
            }
            fillViewbag("Edit", cITA.PACIENTE_ID, cITA.TIPO_CITA_ID);
            ModelState.AddModelError(string.Empty, "No se puede poner dos citas al mismo dia para un paciente");
            return View(cITA);
        }

        // GET: CITA/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CITA cita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Citas/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<CITA>();
                    readTask.Wait();

                    cita = readTask.Result;
                }
            }
            fillViewbag("Delete", cita.PACIENTE_ID, cita.TIPO_CITA_ID);
            return View(cita);
        }

        // POST: CITA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            CITA cita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Citas/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<CITA>();
                    readTask.Wait();

                    cita = readTask.Result;
                }
            }


            if (checkCita24hours(cita))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP DELETE
                    var deleteTask = client.DeleteAsync("Citas/" + id.ToString());
                    deleteTask.Wait();

                    var result = deleteTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        return RedirectToAction("Index");
                    }
                }
                return RedirectToAction("Index");
            }
            fillViewbag("Delete", cita.PACIENTE_ID, cita.TIPO_CITA_ID);
            ModelState.AddModelError(string.Empty, "No se puede borrar cita con menos de 24 horas de antelacion");
            return View(cita);
        }

        public void fillViewbag(string view,int? pId = null, int? tcId = null)
        {
            if (view == "Create")
            {
                ViewBag.PACIENTE_ID = new SelectList(getAllPaciente(), "PACIENTE_ID", "NOMBRE");
                ViewBag.TIPO_CITA_ID = new SelectList(getAllTipo_Cita(), "TIPO_CITA_ID", "TIPO");
            }
            else {
                ViewBag.PACIENTE_ID = new SelectList(getAllPaciente(), "PACIENTE_ID", "NOMBRE", pId);
                ViewBag.TIPO_CITA_ID = new SelectList(getAllTipo_Cita(), "TIPO_CITA_ID", "TIPO", tcId);
            }
        }

        public List<TIPO_CITA> getAllTipo_Cita()
        {
            IEnumerable<TIPO_CITA> tipocita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("TipoCitas");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<TIPO_CITA>>();
                    readTask.Wait();

                    tipocita = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    tipocita = Enumerable.Empty<TIPO_CITA>();

                   
                }
            }
            return tipocita.ToList();
        }

        public List<PACIENTE> getAllPaciente()
        {
            IEnumerable<PACIENTE> paciente = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pacientes");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<PACIENTE>>();
                    readTask.Wait();

                    paciente = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    paciente = Enumerable.Empty<PACIENTE>();


                }
            }
            return paciente.ToList();
        }

        public List<CITA> getAllCita()
        {
            IEnumerable<CITA> cita = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Citas");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<CITA>>();
                    readTask.Wait();

                    cita = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    cita = Enumerable.Empty<CITA>();


                }
            }
            return cita.ToList();
        }

        public bool checkCitaSamePaciente(CITA cita)
        {
            List<CITA> CitaList = getAllCita();

            if (CitaList.Where(x => x.FECHA == cita.FECHA && x.PACIENTE_ID == cita.PACIENTE_ID && x.CITA_ID != cita.CITA_ID).Count() <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkCita24hours(CITA cita)
        {

            if (DateTime.Now > cita.FECHA.Value.AddHours(-24) && DateTime.Now < cita.FECHA.Value)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
