﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PlantillaClinicaClient.Models;
using System.Net.Http;

namespace PlantillaClinicaClient.Controllers
{
    public class PACIENTEController : Controller
    {
        
        // GET: PACIENTE
        public ActionResult Index()
        {
            IEnumerable<PACIENTE> paciente = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pacientes");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<PACIENTE>>();
                    readTask.Wait();

                    paciente = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    paciente = Enumerable.Empty<PACIENTE>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(paciente);
        }


        // GET: PACIENTE/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PACIENTE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PACIENTE_ID,NOMBRE,APELLIDO,TELEFONO,CORREO")] PACIENTE pACIENTE)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP POST
                    var postTask = client.PostAsJsonAsync<PACIENTE>("Pacientes", pACIENTE);
                    postTask.Wait();

                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(pACIENTE);
        }

        // GET: PACIENTE/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PACIENTE paciente = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pacientes/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<PACIENTE>();
                    readTask.Wait();

                    paciente = readTask.Result;
                }
            }

            return View(paciente);
        }

        // POST: PACIENTE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PACIENTE_ID,NOMBRE,APELLIDO,TELEFONO,CORREO")] PACIENTE pACIENTE)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:59866/api/");

                    //HTTP POST
                    var putTask = client.PutAsJsonAsync<PACIENTE>("Pacientes", pACIENTE);
                    putTask.Wait();

                    var result = putTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        return RedirectToAction("Index");
                    }
                }
            }
            return View(pACIENTE);
        }

        // GET: PACIENTE/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PACIENTE paciente = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pacientes/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<PACIENTE>();
                    readTask.Wait();

                    paciente = readTask.Result;
                }
            }

            return View(paciente);
        }

        // POST: PACIENTE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:59866/api/");

                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Pacientes/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }

    }
}
