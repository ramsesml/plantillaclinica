﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PlantillaClinica.Models;
using PlantillaClinica.Repositories;

namespace PlantillaClinica.Controllers
{
    public class CitasController : ApiController
    {
        private CitaRepository db = new CitaRepository();

        // GET: api/Citas
        public async Task<IHttpActionResult> GetCITA()
        {
            return Ok(await db.GetAll());
        }

        // GET: api/Citas/5
        [ResponseType(typeof(CITA))]
        public async Task<IHttpActionResult> GetCITA(int id)
        {
            CITA cITA = await db.Get(id);
            if (cITA == null)
            {
                return NotFound();
            }

            return Ok(cITA);
        }

        // PUT: api/Citas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCITA(CITA cITA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Edit(cITA);

            try
            {
                await db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CITAExists(cITA.CITA_ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Citas
        [ResponseType(typeof(CITA))]
        public async Task<IHttpActionResult> PostCITA(CITA cITA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Add(cITA);
            await db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cITA.CITA_ID }, cITA);
        }

        // DELETE: api/Citas/5
        [ResponseType(typeof(CITA))]
        public async Task<IHttpActionResult> DeleteCITA(int id)
        {
            CITA cITA = await db.Get(id);
            if (cITA == null)
            {
                return NotFound();
            }

            db.Remove(cITA);
            await db.SaveChanges();

            return Ok(cITA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CITAExists(int id)
        {
            Task<List<CITA>> CitaList = db.GetAll();
            return CitaList.Result.Count(e => e.PACIENTE_ID == id) > 0;
        }
    }
}