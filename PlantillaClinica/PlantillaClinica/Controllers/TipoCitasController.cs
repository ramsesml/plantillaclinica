﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PlantillaClinica.Models;
using PlantillaClinica.Repositories;


namespace PlantillaClinica.Controllers
{
    public class TipoCitasController : ApiController
    {
        private TipoCitaRepository db = new TipoCitaRepository();

        // GET: api/TipoCitas
        public async Task<IHttpActionResult> GetTIPO_CITA()
        {
            return Ok(await db.GetAll());
        }

        // GET: api/TipoCitas/5
        [ResponseType(typeof(TIPO_CITA))]
        public async Task<IHttpActionResult> GetTIPO_CITA(int id)
        {
            TIPO_CITA tIPO_CITA = await db.Get(id);
            if (tIPO_CITA == null)
            {
                return NotFound();
            }

            return Ok(tIPO_CITA);
        }

        // PUT: api/TipoCitas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTIPO_CITA(TIPO_CITA tIPO_CITA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Edit(tIPO_CITA);

            try
            {
                await db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TIPO_CITAExists(tIPO_CITA.TIPO_CITA_ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoCitas
        [ResponseType(typeof(TIPO_CITA))]
        public async Task<IHttpActionResult> PostTIPO_CITA(TIPO_CITA tIPO_CITA)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Add(tIPO_CITA);
            await db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tIPO_CITA.TIPO_CITA_ID }, tIPO_CITA);
        }

        // DELETE: api/TipoCitas/5
        [ResponseType(typeof(TIPO_CITA))]
        public async Task<IHttpActionResult> DeleteTIPO_CITA(int id)
        {
            TIPO_CITA tIPO_CITA = await db.Get(id);
            if (tIPO_CITA == null)
            {
                return NotFound();
            }

            db.Remove(tIPO_CITA);
            await db.SaveChanges();

            return Ok(tIPO_CITA);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TIPO_CITAExists(int id)
        {
            Task<List<TIPO_CITA>> TipoCitaList = db.GetAll();
            return TipoCitaList.Result.Count(e => e.TIPO_CITA_ID == id) > 0;
        }

    }
}