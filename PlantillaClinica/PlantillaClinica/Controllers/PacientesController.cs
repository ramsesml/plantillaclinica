﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PlantillaClinica.Models;
using PlantillaClinica.Repositories;

namespace PlantillaClinica.Controllers
{
    public class PacientesController : ApiController
    {
        private PacienteRepository db = new PacienteRepository();

        // GET: api/Pacientes
        public async Task<IHttpActionResult> GetPACIENTE()
        {
         
            return Ok(await db.GetAll());
        }

        // GET: api/Pacientes/5
        [ResponseType(typeof(PACIENTE))]
        public async Task<IHttpActionResult> GetPACIENTE(int id)
        {
            PACIENTE pACIENTE = await db.Get(id);
            if (pACIENTE == null)
            {
                return NotFound();
            }

            return Ok(pACIENTE);
        }

        // PUT: api/Pacientes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPACIENTE(PACIENTE pACIENTE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Edit(pACIENTE);

            try
            {
                await db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PACIENTEExists(pACIENTE.PACIENTE_ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pacientes
        [ResponseType(typeof(PACIENTE))]
        public async Task<IHttpActionResult> PostPACIENTE(PACIENTE pACIENTE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Add(pACIENTE);
            await db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pACIENTE.PACIENTE_ID }, pACIENTE);
        }

        // DELETE: api/Pacientes/5
        [ResponseType(typeof(PACIENTE))]
        public async Task<IHttpActionResult> DeletePACIENTE(int id)
        {
            PACIENTE pACIENTE = await db.Get(id);
            if (pACIENTE == null)
            {
                return NotFound();
            }

            db.Remove(pACIENTE);
            await db.SaveChanges();

            return Ok(pACIENTE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PACIENTEExists(int id)
        {
            Task<List<PACIENTE>> PacienteList = db.GetAll();
            return PacienteList.Result.Count(e => e.PACIENTE_ID == id) > 0;
        }
    }
}