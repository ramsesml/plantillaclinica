﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using PlantillaClinica.Models;

namespace PlantillaClinica.Repositories
{
    public class Repository<T> where T : class
    {
        protected PlantillaClinicaEntities context = null;
        protected DbSet<T> DbSet { get; set; }

        public Repository()
        {
            
                
                context = new PlantillaClinicaEntities();
                DbSet = context.Set<T>();
            
        }

        public Task<List<T>> GetAll()
        {
            return DbSet.ToListAsync();
        }

        public Task<T> Get(int? id)
        {
            return DbSet.FindAsync(id);
        }

        public void Add(T pEntity)
        {
            DbSet.Add(pEntity);
        }

        public void AddRange(List<T> pEntity)
        {
            DbSet.AddRange(pEntity);
        }

        public void Edit(T pEntity)
        {
            context.Entry(pEntity).State = EntityState.Modified;
        }

        public void Remove(T pEntity)
        {
            DbSet.Remove(pEntity);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public Task<int> SaveChanges()
        {
            return context.SaveChangesAsync();
        }

    }
}